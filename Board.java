enum Tile {
	Blank("_"),
	X("X"),
	O("O");
	
	private String name;
	
	Tile(String name) {
        this.name = name;	
    }
	public String getName () {
		return name; 
	} 
	
}

public class Board {
	private Tile[][] grid; 
		
	
		
		public Board() {
        grid = new Tile[3][3];
        for (int r = 0;r < grid.length; r++) {
            for (int c = 0; c < grid[r].length; c++) {
                grid[r][c] = Tile.Blank;
            }
        }
    }
	@Override
	public String toString() {
		String result = "";

    	for (Tile[] r : grid) {
			for (Tile tile : r) {
				result += tile.getName() + " ";
       	 	}
			result += "\n";
   	 	}
		return result;
	}

	public boolean placeToken (int row, int col, Tile playerToken) {
		if (row < 0 || row >= 3 || col < 0 || col >= 3){
			return false;
		}

		if ((grid[row][col] == Tile.Blank)) {
			grid[row][col] = playerToken;
        	return true;
		} else {
			return false;
		}
	}
	public boolean checkIfFull() {
		for (Tile[] r : grid) {
			for (Tile tile : r) {
				if (tile == Tile.Blank) {
					return false; 
				}
			}
		}
		return true;
	}
	private boolean checkIfWinningHorizontal(Tile playerToken) {
		for (int r = 0; r < grid.length; r++) {
			boolean isWinningRow = true;
			for (int c = 0; c < grid[r].length; c++) {
				if (grid[r][c] != playerToken) {
					isWinningRow = false;
				}
			}
			if (isWinningRow) {
				return true; 
			}
		}
		return false; 

	}
	private boolean checkIfWinningVertical(Tile playerToken) {
		for (int c = 0; c < grid.length; c++) {
			boolean isWinningColumn = true;
			for (int r = 0; r < grid.length; r++) {
				if (grid[r][c] != playerToken) {
					isWinningColumn = false;
				}
			}
			if (isWinningColumn) {
				return true; 
			}
		}
		return false; 

	}
	public boolean checkIfWinning(Tile playerToken) {
		if (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken)) {
			return true;
		}
		return false;
    }
	private boolean checkIfWinningDiagonal(Tile playerToken) {
		boolean isWinningFirstDiagonal = true;
		for (int i = 0; i < grid.length; i++) {
			if (grid[i][i] != playerToken) {
				isWinningFirstDiagonal = false;
			}
		}
		if (isWinningFirstDiagonal) {
			return true;
		}
		
		boolean isWinningSecondDiagonal = true;
		for (int i = 0; i < grid.length; i++) {
			if (grid[i][grid.length - 1 - i] != playerToken) {
				isWinningSecondDiagonal = false;
			}
		}
		return isWinningSecondDiagonal;
	}





}

		