import java.util.Scanner;

public class TicTacToeApp {
    public static void main(String[] args) {
        System.out.println("Let's play Tic-Tac-Toe!");
        Board board = new Board();

        boolean gameOver = false;
        int player = 1;
        Tile playerToken = Tile.X;
        
        while (!gameOver) {
            System.out.println(board);
            Scanner scanner = new Scanner(System.in);
            System.out.print("Player " + player + ", enter the row (1-3): ");
            int row = scanner.nextInt() -1;
            System.out.print("Player " + player + ", enter the column (1-3): ");
            int column = scanner.nextInt() -1;

            if (player == 1) {
                playerToken = Tile.X;
            } else {
                playerToken = Tile.O;
            }

            boolean validInput = false;
            validInput = board.placeToken(row, column, playerToken);

            if (!validInput) {
                System.out.println("Invalid input! Please choose an empty position.");
            } else if (board.checkIfWinning(playerToken)) {
                System.out.println("Player " + player + " is the winner!");
                gameOver = true;
            } else if (board.checkIfFull()) {
                System.out.println("It's a tie!");
                gameOver = true;
            } else {
                player++;
                if (player > 2) {
                player = 1;
                }
            }
        }    
    }
}